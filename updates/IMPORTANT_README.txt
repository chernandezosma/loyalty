If your *current* version is 1.5.0 or higher, ignore the file incremental-update-1.5.0.zip.

If your *current* version is 1.8.0 or higher, ignore the files incremental-update-1.5.0.zip and incremental-update-1.8.0.zip.

You only have to overwrite the files in the zip file named with a higher version.