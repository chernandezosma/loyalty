-- Normal database
CREATE DATABASE IF NOT EXISTS `xnet_loyalty` DEFAULT
    CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

CREATE USER IF NOT EXISTS `dbuser`@`%` IDENTIFIED BY 'dbuser';
GRANT ALL PRIVILEGES ON xnet_loyalty.* TO `dbuser`@`%`;

SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');
FLUSH PRIVILEGES;
